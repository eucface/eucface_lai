

facerain <- get_rain("rawmean")
facerain$Date <- as.Date(facerain$DateTime)

x <- subset(facerain, Date > as.Date("2016-1-1"))
with(x, plot(DateTime, cumsum(Rain_mm_Tot), type='l'))
