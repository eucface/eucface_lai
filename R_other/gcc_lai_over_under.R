
source("load.r")

# GCC (overstorey, understorey), LAI

library(tidyr)
library(extrafont)
library(zoo)

# Understorey GCC
gccunder <- read.csv("data/gcctotal_10_40_tsdawn_final.csv") %>%
  rename(Date=ymd) %>%
  separate(site, c("Ring","Camera")) %>%
  mutate(Ring = as.factor(gsub("ring", "R", Ring)),
         Date = as.Date(Date)) %>%
  dplyr::select(Date, everything(), -X) %>%
  group_by(Date, Ring) %>%
  dplyr::summarise(GCC=mean(GCC, na.rm=TRUE)) %>%
  left_join(eucface(), by="Ring")


# library(ggplot2)
# ggplot(gccunder, aes(x=Date, y=GCC, group=Ring)) + geom_smooth(span=0.15, n=501) + theme_minimal()


gccunder_mean <- group_by(gccunder, Date) %>%
  dplyr::summarize(GCC = mean(GCC, na.rm=TRUE)) %>%
  as.data.frame

gccunder_co2 <- group_by(gccunder, Date, treatment) %>%
  dplyr::summarize(GCC = mean(GCC, na.rm=TRUE)) %>%
  as.data.frame


biomunder <- read.csv("data/EucFACE_vegplots_HEIGHTS_AND_PREDICTED_ABOVEGROUNDBIOMASS_v9.csv") %>%
  mutate(Ring = paste0("R", ring),
         Date = as.Date(date)) %>%
  group_by(Ring, Date) %>%
  dplyr::summarize(AGB = mean(AGBpred, na.rm=TRUE)) %>%
  left_join(eucface(), by="Ring")

biomunder_co2 <- group_by(biomunder, treatment, Date) %>%
  dplyr::summarize(AGB = mean(AGB, na.rm=TRUE))


load("cache/lai_workspace.RData")

laismooth <- makesmoothLAI2(facegap_cloudy_byring) %>%
  left_join(eucface(), by="Ring")

laimean <- group_by(facegap_cloudy_byring, Date) %>%
  dplyr::summarize(LAI = mean(LAI)) %>%
  as.data.frame

laico2 <- group_by(facegap_cloudy_byring, Date, treatment) %>%
  dplyr::summarize(LAI = mean(LAI)) %>%
  as.data.frame

gccover <- readRDS("c:/repos/facesecurecam_analysis/cache/gcc_full.rds") %>%
  filter(Fdiff > 0.98) %>%
  mutate(Ring = paste0("R", Ring)) %>%
  group_by(Ring, Date) %>%
  dplyr::summarize(GCC = mean(GCC), RCC=mean(RCC)) %>%
  left_join(eucface(), by="Ring") %>%
  as.data.frame

gccover_mean <- group_by(gccover, Date) %>%
  dplyr::summarize(GCC = mean(GCC), RCC=mean(RCC)) %>%
  as.data.frame

gccover_co2 <- group_by(gccover, Date, treatment) %>%
  dplyr::summarize(GCC = mean(GCC), RCC=mean(RCC)) %>%
  as.data.frame

# Zoomed ones (to check if there is still a CO2 effect)
gccover2 <- readRDS("c:/repos/facesecurecam_analysis/cache/gcc_zoom.rds") %>%
  filter(Fdiff > 0.98) %>%
  mutate(Ring = paste0("R", Ring)) %>%
  group_by(Ring, Date) %>%
  dplyr::summarize(GCC = mean(GCC)) %>%
  left_join(eucface(), by="Ring") %>%
  as.data.frame

gccover2_mean <- group_by(gccover2, Date) %>%
  dplyr::summarize(GCC = mean(GCC)) %>%
  as.data.frame

gccover2_co2 <- group_by(gccover2, Date, treatment) %>%
  dplyr::summarize(GCC = mean(GCC)) %>%
  as.data.frame


rain <- filter(faceraindaily, Date >= as.Date("2010-6-1")) 
x <- rain$Rain.ROS
x[is.na(x)] <- 0
r <- rollsum(x, k=60, na.rm=T, na.pad=T, align="left")


windows(8,6)
par(mar=c(3,5,1,5), family="Gotham Narrow Book", yaxs="i")
xl <- as.Date(c("2014-5-1", "2017-10-1"))
with(laimean, plot(Date, LAI, pch=16, cex=0.6, col="#1f78b4", 
                   xlab="",ylab="",
                   panel.first={
                     xat <- seq(as.Date("2013-1-1"), as.Date("2018-1-1"), by="1 year")
                     abline(v=xat, col="grey", lty=2)
                   },
                   xlim=xl, ylim=c(0,2.3), axes=FALSE))
timeseries_axis()
axis(2, las=1, at=seq(1.4, 2.2, by=0.2), tcl=0.1, line=1, cex.axis=0.8, mgp=c(2,0.3, 0))
mtext(side=2, text="Tree canopy LAI", line=3, at=1.8, cex=1.3)
l <- loess(LAI ~ as.numeric(Date), data=laimean, span=0.12)
plot_loess(l, add=TRUE, n=501, lines.col="#1f78b4", lwd=2)
par(new=TRUE)
with(gccunder_mean, plot(Date, GCC, pch=16, cex=0.4, col="#b2df8a", 
                         ylim=c(0.30, 0.5),
                         ann=FALSE, axes=FALSE, xlim=xl))
l <- loess(GCC ~ as.numeric(Date), data=gccunder_mean, span=0.12)
plot_loess(l, add=TRUE, n=501, lines.col="olivedrab4", lwd=2)
axis(4, at=seq(0.32, 0.36, by=0.02), cex.axis=0.7, line=0.5, tcl=0.1, las=1, mgp=c(2,0.2,0))

par(new=TRUE)
with(gccover_mean, plot(Date, GCC, pch=16, cex=0.4, col="#a6cee3", 
                         ylim=c(0.31-0.055, 0.5-0.055),
                         ann=FALSE, axes=FALSE, xlim=xl))
l <- loess(GCC ~ as.numeric(Date), data=gccover_mean, span=0.12)
plot_loess(l, add=TRUE, n=501, lines.col="cornflowerblue", lwd=2) 
axis(4, at=seq(0.32, 0.36, by=0.02), line=0.5, cex.axis=0.7, tcl=0.1, las=1, mgp=c(2,0.2,0))
text(par("usr")[2] + 180, 0.315, "Greenness", xpd=TRUE, srt=270, cex=1.3)

text(16285.84, 0.3671822, "Canopy", font=2, cex=1.2, col="cornflowerblue")
text(16285.84, 0.3059346, "Understorey", font=2, cex=1.2, col="olivedrab4")
     
par(new=TRUE)
# plot(rain$Date, r, type='l', 
#      xlim=xl, 
#      ylim=c(0,1500), ann=FALSE, axes=FALSE)

cols <- colorRampPalette(c("red", "blue2"))(5)
raincols <- alpha(cols[cut(r, 5)], 0.4)
plot(rain$Date, rep(1, nrow(rain)), col=raincols, type='h',lwd=2,
     ann=FALSE, axes=FALSE,
     xlim=xl, ylim=c(0,30))
box()

dev.copy2pdf(file="output/figures_other/gcc_lai_over_under.pdf")
     






# LAI by CO2
windows(7,5)
par(mar=c(3,5,1,1), family="Gotham Narrow Book", yaxs="i", cex.lab=1.2)
xl <- as.Date(c("2012-10-1", "2017-10-1"))
palette(c("blue2", "red2"))
with(laico2, plot(Date, LAI, pch=16, cex=0.6, col=as.factor(treatment), 
                   xlab="",ylab=expression(Tree~canopy~LAI~~(m^2~m^-2)),
                   panel.first={
                     xat <- seq(as.Date("2013-1-1"), as.Date("2018-1-1"), by="1 year")
                     abline(v=xat, col="grey", lty=2)
                   },
                   xlim=xl, ylim=c(0,2.5), axes=FALSE))
timeseries_axis()
axis(2)

laiamb <- subset(laico2, treatment == "ambient")
l <- loess(LAI ~ as.numeric(Date), data=laiamb, span=0.12)
plot_loess(l, add=TRUE, n=501, lines.col="blue2", lwd=2, band=FALSE)
laiele <- subset(laico2, treatment == "elevated")
l <- loess(LAI ~ as.numeric(Date), data=laiele, span=0.12)
plot_loess(l, add=TRUE, n=501, lines.col="red2", lwd=2, band=FALSE)
legend("bottomright", c("Ambient","Elevated"), lty=1, col=c("blue2","red2"), lwd=2, 
       title=expression(CO[2]~treatment), box.col="white", bg="white", inset=0.01)
dev.copy2pdf(file="output/figures_other/laibyco2_timeseries_loess.pdf")





# GCC canopy by CO2
windows(7,5)
par(mar=c(3,5,1,1), family="Gotham Narrow Book", yaxs="i", cex.lab=1.2)
with(gccover_co2, plot(Date, GCC, pch=16, cex=0.4, col=c("blue2","red2")[treatment],
                       panel.first={
                         xat <- seq(as.Date("2013-1-1"), as.Date("2018-1-1"), by="1 year")
                         abline(v=xat, col="grey", lty=2)
                       },
                       ylab="Tree canopy greenness (-)",
                       ylim=c(0.33, 0.39),
                       xlab="", axes=F))
gccamb <- subset(gccover_co2, treatment == "ambient")
l <- loess(GCC ~ as.numeric(Date), data=gccamb, span=0.12)
plot_loess(l, add=TRUE, n=501, lines.col="blue2", lwd=2, band=FALSE) 

gccele <- subset(gccover_co2, treatment == "elevated")
l <- loess(GCC ~ as.numeric(Date), data=gccele, span=0.12)
plot_loess(l, add=TRUE, n=501, lines.col="red2", lwd=2, band=FALSE) 

timeseries_axis()
axis(2)
box()
legend("topright", c("Ambient","Elevated"), lty=1, col=c("blue2","red2"), lwd=2, 
       title=expression(CO[2]~treatment), box.col="white", bg="white", inset=0.01)
dev.copy2pdf(file="output/figures_other/gccoverbyco2_timeseries_loess.pdf")


# GCC understorey
windows(7,5)
par(mar=c(3,5,1,5), family="Gotham Narrow Book", yaxs="i", 
    cex.lab=1.2, cex.axis=0.8, las=1, tcl=0.2,
    mgp=c(3,0.5,0))
xl <- as.Date(c("2014-7-1","2017-7-1"))
with(gccunder_co2, plot(Date, GCC, pch=16, cex=0.4, col=c("blue2","red2")[treatment],
                        xlim=xl,
                       panel.first={
                         xat <- seq(as.Date("2013-1-1"), as.Date("2018-1-1"), by="1 year")
                         abline(v=xat, col="grey", lty=2)
                       },
                       ylab="Understorey greenness (-)",
                       ylim=c(0.2, 0.41),
                       xlab="", axes=F))
gccamb <- subset(gccunder_co2, treatment == "ambient")
l <- loess(GCC ~ as.numeric(Date), data=gccamb, span=0.12)
plot_loess(l, add=TRUE, n=501, lines.col="blue2", lwd=2, band=FALSE) 

gccele <- subset(gccunder_co2, treatment == "elevated")
l <- loess(GCC ~ as.numeric(Date), data=gccele, span=0.12)
plot_loess(l, add=TRUE, n=501, lines.col="red2", lwd=2, band=FALSE) 

timeseries_axis()
axis(2, at=seq(0.3, 0.4, by=0.02))
box()
legend("bottomright", c("Ambient","Elevated"), lty=1, col=c("blue2","red2"), lwd=2, 
       title=expression(CO[2]~treatment), box.col="white", bg="white", inset=0.01)

par(new=TRUE)
biomunder_co2_2 <- filter(biomunder_co2, Date < as.Date("2017-7-1"))
with(biomunder_co2, plot(Date, AGB, pch=15, cex=0.5,
                         xlim=xl,
                         ylim=c(0,1),
                         col=c("blue2","red2")[treatment],
                         ann=FALSE, axes=FALSE))

biomunder_2 <- filter(biomunder, Date < as.Date("2017-7-1"))
l <- loess(AGB ~ as.numeric(Date), data=biomunder, span=0.3)
dfr <- data.frame(Date = seq(min(biomunder_2$Date), max(biomunder_2$Date), by="1 day"))
dfr$AGBpred <- predict(l, dfr)
with(dfr, lines(Date, AGBpred))

axis(4, at=seq(0,0.5, by=0.1))
par(las=0)
mtext(side=4, line=3, text=expression(Understorey~biomass~(kg~m^-2)), cex=1.2)
dev.copy2pdf(file="output/figures_other/gccunderbyco2_timeseries_loess.pdf")




# Mean LAI only
windows(8,6)
par(mar=c(3,5,1,1), family="Gotham Narrow Book", yaxs="i")
xl <- as.Date(c("2012-6-1", "2017-10-1"))
with(laimean, plot(Date, LAI, pch=16, cex=0.6, col="#1f78b4", 
                   xlab="",ylab="",
                   panel.first={
                     xat <- seq(as.Date("2013-1-1"), as.Date("2018-1-1"), by="1 year")
                     abline(v=xat, col="grey", lty=2)
                   },
                   xlim=xl, ylim=c(0,2.3), axes=FALSE))
timeseries_axis()
l <- loess(LAI ~ as.numeric(Date), data=laimean, span=0.12)
plot_loess(l, add=TRUE, n=501, lines.col="#1f78b4", lwd=2)
box()

rain <- filter(faceraindaily, Date >= as.Date("2010-6-1")) 
x <- rain$Rain.ROS
x[is.na(x)] <- 0
r <- rollsum(x, k=60, na.rm=T, na.pad=T)

par(new=TRUE)
# plot(rain$Date, r, type='l', 
#      xlim=xl, 
#      ylim=c(0,1500), ann=FALSE, axes=FALSE)

cols <- colorRampPalette(c("red", "blue2"))(10)
raincols <- cols[cut(r, 10)]
plot(rain$Date, rep(1, nrow(rain)), col=raincols, type='h',
     xlim=xl, ylim=c(0,10))






