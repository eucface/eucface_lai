

dfr <- facegap_cloudy_byring[,c("Date","Ring","treatment","Gapfraction.mean","LAI")]
names(dfr)[4] <- "fPAR"
dfr$fPAR <- 1 - dfr$fPAR

write.csv(dfr, sprintf("output/FACE_fPAR_byring_20121026_%s.csv",format(max(dfr$Date),"%Y%m%d")),
                       row.names=FALSE)

