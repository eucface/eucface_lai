
# Does it matter (for df, and significance) whether we average sensors by ring,
# and run lmer with (1|Ring), or keep sensor data and run (1|Ring/Sensor) ?
# No it does not, except sensor-level model probably won't fit (or will take ages) for 
# the whole dataset.
# For small subset (see below), Numdf identical, DenDF both large (but not same), p-values v. similar.

facepar_cloudy_bysensor <- makeCloudy(facepar,
                             Fdiff_cutoff= 0.98,
                             PARabovecutoff = 250,
                             PARbelowcutoff = 1300,
                             minSolarElevation = 10,
                             bysensor=TRUE)
df <- facepar_cloudy_bysensor[,c("Date","Ring","Gapfraction1","Gapfraction2","Gapfraction3")]
df <- merge(df, eucface())
names(df)[3:5] <- c("Gapfraction.1","Gapfraction.2","Gapfraction.3")

dfw <- reshape(df, direction="long", timevar="Sensor", varying=3:5)
dfw$id <- NULL

# By sensor
df1 <- summaryBy(. ~ Date + Sensor + Ring, FUN=mean, na.rm=TRUE, data=dfw, keep.names=TRUE,
                 id= ~ treatment)

# by ring
df2 <- summaryBy(. ~ Date + Ring, FUN=mean, na.rm=TRUE, data=dfw, keep.names=TRUE,
                 id= ~ treatment)
df2$Sensor <- NULL

df1$Datef <- as.factor(df1$Date)
df2$Datef <- as.factor(df2$Date)

df1 <- subset(df1, Date < as.Date("2013-2-1"))
df2 <- subset(df2, Date < as.Date("2013-2-1"))

library(lme4)


m1 <- lmer(Gapfraction ~ treatment * Datef + (1|Ring/Sensor), data=df1)
m2 <- lmer(Gapfraction ~ treatment * Datef + (1|Ring), data=df2)

anova(m1)
anova(m2)