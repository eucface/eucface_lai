# EucFACE code base : leaf area index from canopy transmittance

This repository contains the code to process raw data from the HIEv and estimate daily canopy gap fraction and leaf area index (LAI) at the EucFACE. To be able to execute the code yourself, you need to be affiliated with the Hawkesbury Institute for the Environment, and have an active account on the HIEv. 

This code is executed daily on a computer at EucFACE, and results uploaded to HIEv: 
- `FACE_P0037_RA_GAPFRACLAI_OPEN_L2.dat` (near-daily LAI and gap fraction, cloudy days only)
-  `FACE_P0037_RA_PARAGG_YYYY-MM-DD_YYYY-MM-DD.csv` (homogenized 30min radiation data, monthly chunks)

Methods of data processing, subsetting, and estimation of LAI follow [Duursma et al. 2015 (Glob. Change Biol)](http://onlinelibrary.wiley.com/doi/10.1111/gcb.13151/full) (see also the [code to generate the publication](https://www.github.com/eucfacelaipaper)).

## Instructions

- Make sure you have installed the [HIEv R package](https://www.bitbucket.org/remkoduursma/hiev).
- In R, execute `source("run.R")`. This will take at least several minutes the first time, but will be much faster on subsequent runs. 
- When setting this up on a new computer, take extra care to install all packages needed (see `run.R`), you will have to try a few times for all packages to install successfully.
- **Note** : this repository downloads many files from the HIEv, these are stored in the directory specified by `setToPath` in `load.R`.


## Contact 

Remko Duursma & Craig McNamara


## Online

See bitbucket.org/eucface for all scripts.
