



loessplot <- function(x,y,g=NULL,data,
                      span=0.5,
                      fitoneline=FALSE,
                      pointcols=NULL,
                      linecols=NULL, 
                      xlab=NULL, ylab=NULL,
                      poly=TRUE,axes=TRUE,
                      polycolor=alpha("lightgrey",0.7),
                      plotit=TRUE, add=FALSE,
                      npred=101, lwd=2,
                      ...){
  

  if(!is.null(substitute(g))){
    data$G <- as.factor(eval(substitute(g),data))
  } else {
    fitoneline <- TRUE
    data$G <- 1
  }
  data$X <- eval(substitute(x),data)
  data$Y <- eval(substitute(y),data)
  data <- droplevels(data)
  
  data <- data[!is.na(data$X) & !is.na(data$Y) & !is.na(data$G),]
  nlev <- length(unique(data$G))
  if(length(polycolor) == 1)polycolor <- rep(polycolor,nlev)
  
  if(class(data$X) == "Date"){
    xDate <- TRUE
    data$X <- as.numeric(data$X)
  } else {
    xDate <- FALSE
  }
  
  if(is.null(pointcols))pointcols <- palette()
  if(is.null(linecols))linecols <- palette()
  
  if(is.null(xlab))xlab <- substitute(x)
  if(is.null(ylab))ylab <- substitute(y)
  
  fit_loess <- function(data, ...){
    
    l <- loess(Y ~ X, data=data, ...)
    return(l)
  }
  
  pred_loess <- function(l, nx=101){
    
    xs <- seq(min(l$x), max(l$x), length=nx)  
    p <- predict(l, data.frame(X=xs), se=TRUE)
    
    return(c(p, list(x=xs)))
  }

  
  if(!fitoneline){
    
    d <- split(data, data$G)
    fits <- lapply(d, fit_loess, span=span)
    
  } else {
    
    fits <- list(fit_loess(data, span=span))
    
  }
  p <- lapply(fits, pred_loess, nx=npred) 
  
  
  if(plotit){
    if(xDate){
      data$X <- as.Date(data$X, origin="1970-1-1")
    }
    
    if(!add){
      with(data, plot(X, Y, axes=FALSE, pch=16, col=pointcols[G],
                      xlab=xlab, ylab=ylab, ...))
      if(axes){
        if(xDate)
          axis.Date(1, data$X)
        else
          axis(1)
        
        axis(2)
      }
      
    } else {
      with(data, points(X, Y, pch=16, col=pointcols[G],
                        ...))
    }
    
  
    for(i in 1:length(p)){
     
       z <- p[[i]]
      
       if(poly)addpoly(z$x, z$fit - 2*z$se.fit, z$fit + 2*z$se.fit, col=polycolor)
       lines(z$x, z$fit, col=linecols[i], lwd=lwd,...)
    
    }
      
    
  }
  
return(invisible(fits)) 
}
  
